module ft232h-go-periph

go 1.14

require (
	local.packages/grideye v0.0.0-00010101000000-000000000000
	periph.io/x/conn/v3 v3.6.9
	periph.io/x/d2xx v0.0.3
	periph.io/x/host/v3 v3.7.0
)

//replace periph.io/x/host/v3 v3.7.0 => github.com/s-mobi01/host v3.7.0.3
replace periph.io/x/host/v3 v3.7.0 => github.com/s-mobi01/host v0.0.0-20211020041449-84832f05c375

replace local.packages/grideye => ./grideye
