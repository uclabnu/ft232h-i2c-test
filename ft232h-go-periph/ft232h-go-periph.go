package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"periph.io/x/conn/v3/driver/driverreg"
	"runtime"

	//"strings"
	"time"

	//"github.com/toi-uc/amg8833"

	"periph.io/x/d2xx"

	"periph.io/x/conn/v3/gpio"
	"periph.io/x/conn/v3/i2c"

	//"periph.io/x/conn/v3/i2c/i2creg"
	"periph.io/x/host/v3"
	"periph.io/x/host/v3/ftdi"

	//"github.com/s-mobi01/conn/gpio"
	//"github.com/s-mobi01/conn/i2c"
	//"github.com/s-mobi01/conn/i2c/i2creg"
	//"github.com/s-mobi01/host"
	//"github.com/s-mobi01/host/ftdi"

	//"local.packages/conn/gpio"
	//"local.packages/conn/i2c"
	//"local.packages/conn/i2c/i2creg"
	//"local.packages/host"
	//"local.packages/host/ftdi"

	grideye "local.packages/grideye"
)

//******************************************************************************
// 起動パラメタ他 定義
//******************************************************************************
var (
	verbose          = flag.Bool("v", false, "verbose mode")
	paraRvsPixel     = flag.Bool("rvs_pxl", false, "Reverse setting of Pixels data")
	paraUseHighPixel = flag.Bool("use_hi_pxl", false, "Use of high digits in Int Pixels data")
	paraUseHighTherm = flag.Bool("use_hi_thm", false, "Use of high digits in Int Thermistor data")
	//addrList = flag.Bool("deviceList", false, "search i2c Device")
)

//******************************************************************************
//
//******************************************************************************
func ftdiDevCheck(d ftdi.Dev) error {
	i := ftdi.Info{}
	d.Info(&i)

	if i.VenID != 0x0403 {
		return errors.New("not FTDI device")
	}

	return nil
}

//******************************************************************************
//
//******************************************************************************
func process(d ftdi.Dev) {
	i := ftdi.Info{}
	d.Info(&i)
	fmt.Printf("  Type:           %s\n", i.Type)
	fmt.Printf("  Vendor ID:      %#04x\n", i.VenID)
	fmt.Printf("  Device ID:      %#04x\n", i.DevID)

	ee := ftdi.EEPROM{}
	if err := d.EEPROM(&ee); err == nil {
		fmt.Printf("  Manufacturer:   %s\n", ee.Manufacturer)
		fmt.Printf("  ManufacturerID: %s\n", ee.ManufacturerID)
		fmt.Printf("  Desc:           %s\n", ee.Desc)
		fmt.Printf("  Serial:         %s\n", ee.Serial)

		h := ee.AsHeader()
		fmt.Printf("  MaxPower:       %dmA\n", h.MaxPower)
		fmt.Printf("  SelfPowered:    %x\n", h.SelfPowered)
		fmt.Printf("  RemoteWakeup:   %x\n", h.RemoteWakeup)
		fmt.Printf("  PullDownEnable: %x\n", h.PullDownEnable)
		switch i.Type {
		case "FT232H":
			p := ee.AsFT232H()
			fmt.Printf("  CSlowSlew:      %t\n", p.ACSlowSlew != 0)        // AC bus pins have slow slew
			fmt.Printf("  CSchmittInput:  %t\n", p.ACSchmittInput != 0)    // AC bus pins are Schmitt input
			fmt.Printf("  CDriveCurrent:  %dmA\n", p.ACDriveCurrent*2)     // Valid values are 4mA, 8mA, 12mA, 16mA in 2mA units
			fmt.Printf("  DSlowSlew:      %t\n", p.ADSlowSlew != 0)        // AD bus pins have slow slew
			fmt.Printf("  DSchmittInput:  %t\n", p.ADSchmittInput != 0)    // AD bus pins are Schmitt input
			fmt.Printf("  DDriveCurrent:  %dmA\n", p.ADDriveCurrent)       // Valid values are 4mA, 8mA, 12mA, 16mA in 2mA units
			fmt.Printf("  Cbus0:          %s\n", p.Cbus0)                  //
			fmt.Printf("  Cbus1:          %s\n", p.Cbus1)                  //
			fmt.Printf("  Cbus2:          %s\n", p.Cbus2)                  //
			fmt.Printf("  Cbus3:          %s\n", p.Cbus3)                  //
			fmt.Printf("  Cbus4:          %s\n", p.Cbus4)                  //
			fmt.Printf("  Cbus5:          %s\n", p.Cbus5)                  //
			fmt.Printf("  Cbus6:          %s\n", p.Cbus6)                  //
			fmt.Printf("  Cbus7:          %s\n", p.Cbus7)                  // C7 is limited a sit can only do 'suspend on C7 low'. Defaults pull down.
			fmt.Printf("  Cbus8:          %s\n", p.Cbus8)                  //
			fmt.Printf("  Cbus9:          %s\n", p.Cbus9)                  //
			fmt.Printf("  FT1248Cpol:     %t\n", p.FT1248Cpol != 0)        // FT1248 clock polarity - clock idle high (true) or clock idle low (false)
			fmt.Printf("  FT1248Lsb:      %t\n", p.FT1248Lsb != 0)         // FT1248 data is LSB (true), or MSB (false)
			fmt.Printf("  FT1248FlowCtrl: %t\n", p.FT1248FlowControl != 0) // FT1248 flow control enable
			fmt.Printf("  IsFifo:         %t\n", p.IsFifo != 0)            // Interface is 245 FIFO
			fmt.Printf("  IsFifoTar:      %t\n", p.IsFifoTar != 0)         // Interface is 245 FIFO CPU target
			fmt.Printf("  IsFastSer:      %t\n", p.IsFastSer != 0)         // Interface is Fast serial
			fmt.Printf("  IsFT1248:       %t\n", p.IsFT1248 != 0)          // Interface is FT1248
			fmt.Printf("  PowerSaveEnabl: %t\n", p.PowerSaveEnable != 0)   // Suspect on ACBus7 low
			fmt.Printf("  DriverType:     %d\n", p.DriverType)             // 0 is D2XX, 1 is VCP
		case "FT2232H":
			p := ee.AsFT2232H()
			fmt.Printf("  ALSlowSlew:      %t\n", p.ALSlowSlew != 0)      // AL pins have slow slew
			fmt.Printf("  ALSchmittInput:  %t\n", p.ALSlowSlew != 0)      // AL pins are Schmitt input
			fmt.Printf("  ALDriveCurrent:  %dmA\n", p.ALDriveCurrent*2)   // Valid values are 4mA, 8mA, 12mA, 16mA in 2mA units
			fmt.Printf("  AHSlowSlew:      %t\n", p.AHSlowSlew != 0)      // AH pins have slow slew
			fmt.Printf("  AHSchmittInput:  %t\n", p.AHSlowSlew != 0)      // AH pins are Schmitt input
			fmt.Printf("  AHDriveCurrent:  %dmA\n", p.AHDriveCurrent*2)   // Valid values are 4mA, 8mA, 12mA, 16mA in 2mA units
			fmt.Printf("  BLSlowSlew:      %t\n", p.ALSlowSlew != 0)      // BL pins have slow slew
			fmt.Printf("  BLSchmittInput:  %t\n", p.ALSlowSlew != 0)      // BL pins are Schmitt input
			fmt.Printf("  BLDriveCurrent:  %dmA\n", p.ALDriveCurrent*2)   // Valid values are 4mA, 8mA, 12mA, 16mA in 2mA units
			fmt.Printf("  BHSlowSlew:      %t\n", p.AHSlowSlew != 0)      // BH pins have slow slew
			fmt.Printf("  BHSchmittInput:  %t\n", p.AHSlowSlew != 0)      // BH pins are Schmitt input
			fmt.Printf("  BHDriveCurrent:  %dmA\n", p.AHDriveCurrent*2)   // Valid values are 4mA, 8mA, 12mA, 16mA in 2mA units
			fmt.Printf("  AIsFifo:         %t\n", p.AIsFifo != 0)         // Interface is 245 FIFO
			fmt.Printf("  AIsFifoTar:      %t\n", p.AIsFifoTar != 0)      // Interface is 245 FIFO CPU target
			fmt.Printf("  AIsFastSer:      %t\n", p.AIsFastSer != 0)      // Interface is Fast serial
			fmt.Printf("  BIsFifo:         %t\n", p.BIsFifo != 0)         // Interface is 245 FIFO
			fmt.Printf("  BIsFifoTar:      %t\n", p.BIsFifoTar != 0)      // Interface is 245 FIFO CPU target
			fmt.Printf("  BIsFastSer:      %t\n", p.BIsFastSer != 0)      // Interface is Fast serial
			fmt.Printf("  PowerSaveEnable: %t\n", p.PowerSaveEnable != 0) // Using BCBUS7 to save power for self-powered designs
			fmt.Printf("  ADriverType:     %t\n", p.ADriverType != 0)     // 0 is D2XX, 1 is VCP
			fmt.Printf("  BDriverType:     %t\n", p.BDriverType != 0)     // 0 is D2XX, 1 is VCP
		case "FT232R":
			p := ee.AsFT232R()
			fmt.Printf("  IsHighCurrent:  %t\n", p.IsHighCurrent != 0) // High Drive I/Os; 3mA instead of 1mA (@3.3V)
			fmt.Printf("  UseExtOsc:      %t\n", p.UseExtOsc != 0)     // Use external oscillator
			fmt.Printf("  InvertTXD:      %t\n", p.InvertTXD != 0)     //
			fmt.Printf("  InvertRXD:      %t\n", p.InvertRXD != 0)     //
			fmt.Printf("  InvertRTS:      %t\n", p.InvertRTS != 0)     //
			fmt.Printf("  InvertCTS:      %t\n", p.InvertCTS != 0)     //
			fmt.Printf("  InvertDTR:      %t\n", p.InvertDTR != 0)     //
			fmt.Printf("  InvertDSR:      %t\n", p.InvertDSR != 0)     //
			fmt.Printf("  InvertDCD:      %t\n", p.InvertDCD != 0)     //
			fmt.Printf("  InvertRI:       %t\n", p.InvertRI != 0)      //
			fmt.Printf("  Cbus0:          %s\n", p.Cbus0)              //
			fmt.Printf("  Cbus1:          %s\n", p.Cbus1)              //
			fmt.Printf("  Cbus2:          %s\n", p.Cbus2)              //
			fmt.Printf("  Cbus3:          %s\n", p.Cbus3)              //
			fmt.Printf("  Cbus4:          %s\n", p.Cbus4)              //
			fmt.Printf("  DriverType:     %d\n", p.DriverType)         // 0 is D2XX, 1 is VCP
		default:
			fmt.Printf("Unknown device:   %s\n", i.Type)
		}
		log.Printf("  Raw: %x\n", ee.Raw)
	} else {
		fmt.Printf("Failed to read EEPROM: %v\n", err)
	}

	if ua, err := d.UserArea(); err != nil {
		fmt.Printf("Failed to read UserArea: %v\n", err)
	} else {
		fmt.Printf("UserArea: %x\n", ua)
	}

	hdr := d.Header()
	for _, p := range hdr {
		fmt.Printf("%s: %s\n", p, p.Function())
	}
}

//******************************************************************************
//
//******************************************************************************
func listD2xx() []ftdi.Dev {
	if !*verbose {
		log.SetOutput(ioutil.Discard)
	}
	log.SetFlags(log.Lmicroseconds)
	if (flag.NArg() != 0) {
		return nil
	}

	major, minor, build := d2xx.Version()
	fmt.Printf("Using library %d.%d.%d\n", major, minor, build)

	var all		[]ftdi.Dev
	var err		error
	var stat	*driverreg.State

	//time.Sleep(500 * time.Millisecond)

	for iTry := 0; iTry < 1; iTry++ {	// リトライテスト用

		stat, err = host.Init()
		if (nil != err) {
			log.Fatal(err)
			//return nil
		}
		if (stat == nil) {
			fmt.Printf("status nil")
		}

		//time.Sleep(500 * time.Millisecond)
		err = nil
		all = ftdi.All()
		//for iCnt, d := range all {
		for iCnt, _ := range all {
			//fmt.Printf("%s\n", d)
			err = ftdiDevCheck(all[iCnt])
			if (nil != err) {
				break	// iCntループ抜け
			}

		}
		if (nil == err) {
			break		// iTryループ抜け
		}
	}

	plural := ""
	if (len(all) > 1) {
		plural = "s"	// 複数形表現「s」付加
	}
	fmt.Printf("Found %d device%s\n", len(all), plural)
	for i, d := range all {
		fmt.Printf("- Device #%d\n", i)
		process(d)
		if (i != (len(all) - 1)) {
			fmt.Printf("\n")
		}
	}
	return all
}

//******************************************************************************
// 温度レジスタ 情報取得（仮対策：goルーチンテストのため関数化）
//******************************************************************************
func readPixels(ge *grideye.GRIDEYE) {

	start := time.Now()
	Pixels, err := ge.ReadPixels()
	if (nil != err) {
		log.Fatalf("grideye.GetPixels(): %v", err)
	}
	end := time.Now()
	fmt.Printf("Pixels GE=%d(0x%02X) time=%s : %g\n", ge.GENum, ge.SlaveAddr, end.Sub(start), Pixels)

	return
}

//******************************************************************************
// サブプロセス呼出し
//******************************************************************************
func SubProcessCall(cmd string) ([]byte, error) {

	sCmdResult, err := exec.Command("sh", "-c", cmd).Output()
	if (nil != err) {
		return nil, err
	}

	return sCmdResult, nil
}

//******************************************************************************
// FTDI ドライバ無効
//******************************************************************************
func disable_FTDI_driver() error {

	os := runtime.GOOS
	switch os {
	case "darwin":
		// エラーチェックしない
		SubProcessCall("kextunload -b com.apple.driver.AppleUSBFTDI")
		SubProcessCall("kextunload /System/Library/Extensions/FTDIUSBSerialDriver.kext")

	case "linux":
		sPass := "uclab"
		sSudo := "echo " + sPass + " | sudo -S "

		// エラーチェックしない(root権限がないとエラーになる（後で検討）)
		SubProcessCall(sSudo + "modprobe -r -q ftdi_sio")
		SubProcessCall(sSudo + "modprobe -r -q usbserial")

	case "windows":
	case "freebsd":
	default: // ガード

	}

	return nil
}

//******************************************************************************
// FTDI ドライバ有効
//******************************************************************************
func enable_FTDI_driver() error {

	os := runtime.GOOS
	switch os {
	case "darwin":
		// エラーチェックしない
		SubProcessCall("kextload -b com.apple.driver.AppleUSBFTDI")
		SubProcessCall("kextload /System/Library/Extensions/FTDIUSBSerialDriver.kext")

	case "linux":
		sPass := "uclab"
		sSudo := "echo " + sPass + " | sudo -S "

		// エラーチェックしない
		SubProcessCall(sSudo + "modprobe -q ftdi_sio")
		SubProcessCall(sSudo + "modprobe -q usbserial")

	case "windows":
	case "freebsd":
	default: // ガード

	}

	return nil
}

//******************************************************************************
// メインルーチン
//******************************************************************************
func main() {
	var err error

	//------------------------------------------------------
	// 起動パラメタ処理
	//------------------------------------------------------
	flag.Parse()

	//------------------------------------------------------
	// FTDIドライバアンロード
	//------------------------------------------------------
	err = disable_FTDI_driver()
	if (nil != err) {
		// sudo rmmod ftdi_sio usbserial
		log.Fatalf("disable_FTDI_driver(): %s", err)
	}
	defer enable_FTDI_driver()

	//------------------------------------------------------
	// FTDI オブジェクト生成
	//------------------------------------------------------
	ok := false
	var ft232h		*ftdi.FT232H
	for iTry := 0; iTry < 1; iTry++ {	// リトライテスト用

		all := listD2xx()

		if (len(all) == 0) {
			log.Fatal("found no FTDI device on the USB bus")
		}

		// Use channel A.
		ft232h, ok = all[0].(*ftdi.FT232H)
		if (ok == true) {
			break;
		}
		//time.Sleep(500 * time.Millisecond)

	}

	if (ok != true) {
		// ubuntuの場合、sudo rmmod ftdi_sio usbserial してないとエラーになる
		log.Fatal("not FTDI device on the USB bus")
	}

	var info ftdi.Info
	ft232h.Info(&info)
	fmt.Printf("%s, ven:%04x, Dev:%04x opend:%v\n", info.Type, info.VenID, info.DevID, info.Opened)

	//------------------------------------------------------
	// FT232H I2C オブジェクト生成
	//------------------------------------------------------
	var i2cdevice i2c.BusCloser
	i2cdevice, err = ft232h.I2C(gpio.Float)
	if err != nil {
		log.Fatalf("ft232h.I2C(): %s", err)
	}

	defer i2cdevice.Close()

	//------------------------------------------------------
	// I2C デバイス検索
	//------------------------------------------------------
	var uiSlaveAddr uint16
	var uiSlaveAddrList []uint16
	for uiSlaveAddr = 0x08; uiSlaveAddr <= 0x77; uiSlaveAddr++ {
		err = i2cdevice.Tx(uiSlaveAddr, []byte{}, []byte{})
		//fmt.Printf("i2cdevice.Tx() rd=%v err=%v\n", rd, err)

		if err != nil {
			continue // for文 先頭へ
		}
		uiSlaveAddrList = append(uiSlaveAddrList, uiSlaveAddr) // アドレスリストに追加
	}
	fmt.Printf("Find I2C Device Count = %d (HEX %02X)\n", len(uiSlaveAddrList), uiSlaveAddrList)

	//------------------------------------------------------
	// grideyeオブジェクト生成
	//------------------------------------------------------
	var iCnt int
	var geCfg *grideye.GEConfig
	var pGEList []*grideye.GRIDEYE

	// config 設定
	geCfg = grideye.GetConfigDefault()
	geCfg.ReversePixel = *paraRvsPixel
	geCfg.UsePixelTempHigh = *paraUseHighPixel
	geCfg.UseThermTempHigh = *paraUseHighTherm

	// grideyeオブジェクト生成
	for iCnt, uiSlaveAddr = range uiSlaveAddrList {
		//log.Printf(" addr %02d: 0x%02x\n", iCnt, uiSlaveAddr)
		ge, err := grideye.New(uint(iCnt), uiSlaveAddr, i2cdevice, geCfg)
		if nil != err {
			log.Fatalf("grideye.New(): %v, GE=%d(0x%02X)", err, iCnt, uiSlaveAddr)
		}

		fmt.Printf("using: %s\n", ge) // GRIDEYE implements String()
		pGEList = append(pGEList, ge) // GRIDEYEリストに追加
	}

	//------------------------------------------------------
	// レジスタ 情報取得
	//------------------------------------------------------
	for { // 無限ループ
		for iCnt = 0; iCnt < len(pGEList); iCnt++ {
			//--------------------------------------------------
			// 温度レジスタ 情報取得
			//--------------------------------------------------
			readPixels(pGEList[iCnt])
			//Pixels, err := pGEList[iCnt].ReadPixels()
			//if (nil != err) {
			//	log.Fatalf("grideye.GetPixels(): %v", err)
			//}
			//log.Printf("Pixels GE=%d(0x%02x) : %g\n", pGEList[iCnt].GENum, pGEList[iCnt].SlaveAddr, Pixels)

			//--------------------------------------------------
			// サーミスタ 情報取得（未使用データ）
			//--------------------------------------------------
			//Therm, err := pGEList[iCnt].ReadThermistor()
			//if (nil != err) {
			//	log.Fatalf("grideye.ReadThermistor(): %v", err)
			//}
			//log.Printf("Therm GE=%d(0x%02x) : %f", pGEList[iCnt].GENum, pGEList[iCnt].SlaveAddr, Therm)
		}
	}

	return
}

//******************************************************************************
// End Of File
//*********************************************
