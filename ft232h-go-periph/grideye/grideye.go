//******************************************************************************
// GRID-Eyeの仕様についてはリファレンスマニュアル参照のこと
// https://docid81hrs3j1.cloudfront.net/medialibrary/2017/11/PANA-S-A0002141979-1.pdf
//******************************************************************************
package grideye

//******************************************************************************
// import
//******************************************************************************
import (
	"fmt"
	"time"

	"periph.io/x/conn/v3/i2c"
)

//******************************************************************************
// 定数 定義
//******************************************************************************
const (
	//--------------------------------------------------------------------------
	// レジスタ アドレス
	//--------------------------------------------------------------------------
	_GE_POWER_CTL_REG = 0x00 // パワーコントロール レジスタ(動作モード設定)
	_GE_RESET_REG     = 0x01 // Writing to reset software.
	_GE_FPSC_REG      = 0x02 // Setting Frame Rate
	_GE_INT_CTL_REG   = 0x03 // Setting Interrupt Function.
	_GE_STAT_REG      = 0x04 // Indicate Overflow Flag and Interrupt Flag
	_GE_SCLR_REG      = 0x05 // Status Clear Register
	_GE_AVE_REG       = 0x07 // Setting moving average Output Mode
	_GE_INTHL_REG     = 0x08 // Interrupt upper value - Upper level
	_GE_INTHH_REG     = 0x09 // Interrupt upper value - Upper level
	_GE_INTLL_REG     = 0x0A // Interrupt lower value - Lower level
	_GE_INTLH_REG     = 0x0B // Interrupt lower value - upper level
	_GE_IHYSL_REG     = 0x0C // Interrupt hysteresis value - Lower level
	_GE_IHYSH_REG     = 0x0D // Interrupt hysteresis value - Lower level
	_GE_TTHL_REG      = 0x0E // Thermistor Output Value - Lower level
	_GE_TTHH_REG      = 0x0F // Thermistor Output Value - Upper level
	_GE_INT0_REG      = 0x10 // Pixel 1->8	 Interrupt Result
	_GE_INT1_REG      = 0x11 // Pixel 9->16	 Interrupt Result
	_GE_INT2_REG      = 0x12 // Pixel 17->24  Interrupt Result
	_GE_INT3_REG      = 0x13 // Pixel 25->32  Interrupt Result
	_GE_INT4_REG      = 0x14 // Pixel 33->40  Interrupt Result
	_GE_INT5_REG      = 0x15 // Pixel 41->48  Interrupt Result
	_GE_INT6_REG      = 0x16 // Pixel 49->56  Interrupt Result
	_GE_INT7_REG      = 0x17 // Pixel 57->64  Interrupt Result
	_GE_AVE_REG2      = 0x1F // Setting moving average Output Mode

	_GE_PIXEL_BASE = 0x80 // 温度レジスタ 基底値 Pixel 1 Output Value (Lower Level)

	//--------------------------------------------------------------------------
	// レジスタ 設定値
	//--------------------------------------------------------------------------
	// パワーコントロール レジスタ(動作モード設定)
	// _GE_POWER_CTL_REG
	_GE_PCTL_NORMAL_MODE       = 0x00
	_GE_PCTL_SLEEEP_MODE       = 0x10
	_GE_PCTL_STAND_BY_60S_MODE = 0x20
	_GE_PCTL_STAND_BY_10S_MODE = 0x21

	// _GE_RESET_REG - Software Resets
	_GE_RST_FLAG_RST    = 0x30
	_GE_RST_INITIAL_RST = 0x3F

	// _GE_FPSC_REG - Sample Rate Settings
	_GE_FPSC_1FPS  = 0x01
	_GE_FPSC_10FPS = 0x00

	// _GE_INT_CTL_REG - Interrupt Settings
	_GE_INTC_ABS = 0b00000001
	_GE_INTC_DIF = 0b00000011
	_GE_INTC_OFF = 0b00000000

	// _GE_AVE_REG - average Settings
	_GE_ABE_NOMAL = 0x00
	_GE_ABE_MAMOD = 0x20

	// _GE_STAT_REG - Overflow/Interrupt Settings
	_GE_SCLR_CLR = 0b00000110

	// _GE_PIXEL_BASE - 温度レジスタ Settings
	_PIXEL_ARRAY_HEIGHT    = 8
	_PIXEL_ARRAY_WIDTH     = 8
	_PIXEL_TEMP_CONVERSION = 0.25
	_THERMISTOR_CONVERSION = 0.0625
)

//******************************************************************************
// グローバル
//******************************************************************************

//******************************************************************************
// 構造体 定義
//******************************************************************************
// GRIDEYE Config
type GEConfig struct {
	ReversePixel     bool // ピクセル（温度レジスタ）データ逆設定
	UsePixelTempHigh bool // ピクセル（温度レジスタ）上位データ 使用有無
	UseThermTempHigh bool // サーミスタレジスタ 上位データ 使用有無
}

// GRIDEYE Object
type GRIDEYE struct {
	GENum     uint
	SlaveAddr uint16
	Config    *GEConfig
	i2c       i2c.BusCloser
}

//******************************************************************************
//
//******************************************************************************
// String constructs a string representation of an GRIDEYE.
func (ge *GRIDEYE) String() string {

	str := fmt.Sprintf("{ GE=%d, SlaveAddr: 0x%02X, Config:{ ReversePixel: %v, UsePixelTempHigh: %v, UseThermTempHigh: %v } }",
		ge.GENum,
		ge.SlaveAddr,
		ge.Config.ReversePixel,
		ge.Config.UsePixelTempHigh,
		ge.Config.UseThermTempHigh)
	return str
}

//******************************************************************************
// コンフィグ初期値取得
//******************************************************************************
func GetConfigDefault() *GEConfig {

	// GRIDEYE Configクラス生成
	cfg := &GEConfig{
		ReversePixel:     false,
		UsePixelTempHigh: false,
		UseThermTempHigh: false,
	}

	return cfg
}

//******************************************************************************
// GRIDEYE オブジェクト生成
//******************************************************************************
func New(num uint, slave uint16, i2c i2c.BusCloser, cfg *GEConfig) (*GRIDEYE, error) {

	// GRIDEYE Configクラス生成
	if nil == cfg {
		cfg = GetConfigDefault()
	}

	// GRIDEYEクラス生成
	ge := &GRIDEYE{
		GENum:     num,
		SlaveAddr: slave,
		Config:    cfg,
		i2c:       i2c,
	}

	// GRIDEYE初期化
	err := ge.init()
	if nil != err {
		return nil, err
	}

	return ge, nil
}

//******************************************************************************
// 温度レジスタ 情報取得
//******************************************************************************
func (ge *GRIDEYE) ReadPixels() ([]float32, error) {

	//var	arPixels			[ _PIXEL_ARRAY_HEIGHT ][ _PIXEL_ARRAY_WIDTH ]float32
	//var	arPixels			[]float32
	arPixels := make([]float32, 0, _PIXEL_ARRAY_HEIGHT*_PIXEL_ARRAY_WIDTH) // キャパシタ確保による高速化
	var iRow int
	var iCol int
	var uiRegOffSet uint8
	var uiReg uint8
	var uiTempRawL uint8
	var uiTempRawH uint8
	var uiTempRawHL uint
	var fTempConv float32
	var err error
	//------------------------------------------------------------------------------
	for iRow = 0; iRow < _PIXEL_ARRAY_HEIGHT; iRow++ { // 行ループ
		for iCol = 0; iCol < _PIXEL_ARRAY_WIDTH; iCol++ { // 列ループ

			// 温度レジスタ アドレス 生成
			uiRegOffSet = uint8(iRow*_PIXEL_ARRAY_HEIGHT + iCol)
			uiReg = _GE_PIXEL_BASE + (uiRegOffSet << 1) // 0x80(T01L), 0x82(T02L), 0x83(T03L) ～ 0xFE(T64L) 一つ跳ばし(下位情報のみ)

			// 温度レジスタ情報 取得(下位)
			uiTempRawL, err = ge.readRegU8(uiReg)
			if nil != err {
				return nil, err
			}

			// 温度レジスタ情報 取得(上位)
			if ge.Config.UsePixelTempHigh == true {
				// 温度レジスタ(上位)情報 使用する場合
				uiTempRawH, err = ge.readRegU8(uiReg + 1)
				if nil != err {
					return nil, err
				}
			} else {
				// 温度レジスタ(上位)情報 使用しない場合（高速対応）
				uiTempRawH = 0
			}

			// 温度レジスタ情報 結合（上位 下位）
			uiTempRawHL = uint((uiTempRawH << 8) | uiTempRawL)

			// 温度レジスタ情報 補正
			fTempConv = ge.twosCompl(uiTempRawHL) * _PIXEL_TEMP_CONVERSION // scaling values 0.25

			// 温度情報 記憶
			//arPixels[iRow][iCol] = fTempConv
			arPixels = append(arPixels, fTempConv) // 温度情報リストに追加
		}
	}

	// ピクセル情報 逆設定？
	if ge.Config.ReversePixel == true {
		arPixels = ge.reversePixels(arPixels)
	}

	return arPixels, nil
}

//******************************************************************************
// サーミスタ 情報取得
//******************************************************************************
func (ge *GRIDEYE) ReadThermistor() (float32, error) {

	var uiThmRawH uint8
	var uiThmRawL uint8
	var uiThmRawHL uint
	var fThmConv float32
	var err error
	//------------------------------------------------------------------------------
	// サーミスタ出力値（下位）取得
	uiThmRawL, err = ge.readRegU8(_GE_TTHL_REG)
	if nil != err {
		return 0, err
	}

	// サーミスタ出力値（上位）取得
	if ge.Config.UseThermTempHigh == true {
		// サーミスタ出力値(上位)情報 使用する場合
		uiThmRawH, err = ge.readRegU8(_GE_TTHH_REG)
		if nil != err {
			return 0, err
		}

		uiThmRawHL = uint((uiThmRawH << 8) | uiThmRawL)

		// サーミスタ出力値 補正
		fThmConv = ge.signedConv(uiThmRawHL) * _THERMISTOR_CONVERSION // scaling values 0.0625

	} else {
		// サーミスタ出力値(上位)情報 使用しない場合（高速対応）
		//uiThmRawH = 0
		fThmConv = float32(uiThmRawL) * _THERMISTOR_CONVERSION // scaling values 0.0625
	}

	return fThmConv, nil
}

//******************************************************************************
// GRIDEYEの初期化を行う
//******************************************************************************
func (ge *GRIDEYE) init() error {

	// 動作モード設定
	err := ge.setOperationMode()
	if nil != err {
		return err
	}

	// warmup delay(ガード)
	time.Sleep(100 * time.Millisecond)

	return nil
}

//******************************************************************************
// GRIDEYE 動作モード設定
//******************************************************************************
func (ge *GRIDEYE) setOperationMode() error {

	var err error

	//----------------------------------
	// パワーコントロール モード設定
	//----------------------------------
	err = ge.writeReg(_GE_POWER_CTL_REG, _GE_PCTL_NORMAL_MODE)
	if nil != err {
		return err
	}
	// warmup delay
	time.Sleep(50 * time.Millisecond)

	//----------------------------------
	// ソフトウェアリセット情報 設定
	//----------------------------------
	err = ge.writeReg(_GE_RESET_REG, _GE_RST_INITIAL_RST)
	if nil != err {
		return err
	}

	// warmup delay
	time.Sleep(2 * time.Millisecond)

	//	err = ge.writeReg(_GE_RESET_REG, _GE_RST_FLAG_RST)
	//	if (nil != err) {
	//		return err
	//	}

	//----------------------------------
	// 割込み モード設定
	//----------------------------------
	err = ge.writeReg(_GE_INT_CTL_REG, _GE_INTC_OFF)
	if nil != err {
		return err
	}

	//----------------------------------
	// フレームレート 設定
	//----------------------------------
	err = ge.writeReg(_GE_FPSC_REG, _GE_FPSC_10FPS)
	if nil != err {
		return err
	}

	//----------------------------------
	// 移動平均出力モード設定
	//----------------------------------
	err = ge.writeReg(_GE_AVE_REG2, 0x50)
	if nil != err {
		return err
	}

	err = ge.writeReg(_GE_AVE_REG2, 0x45)
	if nil != err {
		return err
	}

	err = ge.writeReg(_GE_AVE_REG2, 0x57)
	if nil != err {
		return err
	}

	err = ge.writeReg(_GE_AVE_REG, _GE_ABE_MAMOD)
	if nil != err {
		return err
	}

	err = ge.writeReg(_GE_AVE_REG2, 0x00)
	if nil != err {
		return err
	}

	return nil
}

//******************************************************************************
// レジスタアドレス情報 書込み（8ビット）
//******************************************************************************
func (ge *GRIDEYE) writeReg(RegAddr byte, Value byte) error {

	// コマンド作成
	byCmd := []byte{RegAddr, Value}

	// レジスタアドレス書込み
	err := ge.i2c.Tx(ge.SlaveAddr, byCmd, []byte{})
	if nil != err {
		return err
	}
	//	if (WtSize != uint(len(uiCmd))) {	// コメントを外す時はerrコード設定要！
	//		return err
	//	}

	return nil
}

//******************************************************************************
// レジスタアドレス情報 読込み（8ビット）
//******************************************************************************
func (ge *GRIDEYE) readRegU8(RegAddr byte) (uint8, error) {

	// 受信バッファ確保
	byRcv := []byte{0x00}

	// コマンド作成
	byCmd := []byte{RegAddr}

	// レジスタアドレス読込み
	err := ge.i2c.Tx(ge.SlaveAddr, byCmd, byRcv)
	if nil != err {
		return 0, err
	}

	return uint8(byRcv[0]), nil
}

//******************************************************************************
// conversion for pixels
//******************************************************************************
func (ge *GRIDEYE) twosCompl(RawDt uint) float32 {

	var fConvDt float32

	// 氷点下か?(HIGH側 2^3ビット参照でもよいが他のロジック合わせ)
	if (0x7FF & RawDt) == RawDt {
		fConvDt = float32(RawDt)
	} else {
		fConvDt = float32(RawDt - 4096)
	}

	return fConvDt
}

//******************************************************************************
// conversion for thermistor
//******************************************************************************
func (ge *GRIDEYE) signedConv(RawDt uint) float32 {

	var fConvDt float32

	// 氷点下か?(HIGH側 2^3ビット参照でもよいが他のロジック合わせ)
	if (0x7FF & RawDt) == RawDt {
		fConvDt = float32(RawDt)
	} else {
		fConvDt = -(float32(0x7FF & RawDt))
	}

	return fConvDt
}

//******************************************************************************
// ピクセル情報 逆設定
//******************************************************************************
func (ge *GRIDEYE) reversePixels(rawPxl []float32) []float32 {

	var iCnt int
	iPxlCnt := len(rawPxl)
	arConv := make([]float32, 0, iPxlCnt) // キャパシタ確保による高速化

	for iCnt = iPxlCnt; iCnt > 0; iCnt-- {
		arConv = append(arConv, rawPxl[iCnt-1])
	}

	return arConv
}

//******************************************************************************
// End Of File
//******************************************************************************
