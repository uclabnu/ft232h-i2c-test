# coding: utf-8

import time
import Adafruit_GPIO.FT232H as FT232H

#*******************************************************************************
# GRID-Eyeの仕様についてはリファレンスマニュアル参照のこと
# https://docid81hrs3j1.cloudfront.net/medialibrary/2017/11/PANA-S-A0002141979-1.pdf
#*******************************************************************************

#*******************************************************************************
# 定数 定義
#*******************************************************************************
#-------------------------------------------------------------------------------
# Grid-EYE default Info
#-------------------------------------------------------------------------------time
#GE_I2C_ADDRESS           = 0x68

#-------------------------------------------------------------------------------
# レジスタ アドレス
#-------------------------------------------------------------------------------
GE_POWER_CTL_REG         = 0x00 # パワーコントロール レジスタ(動作モード設定)
GE_RESET_REG             = 0x01 # Writing to reset software. 
GE_FPSC_REG              = 0x02 # Setting Frame Rate
GE_INT_CTL_REG           = 0x03 # Setting Interrupt Function.
GE_STAT_REG              = 0x04 # Indicate Overflow Flag and Interrupt Flag
GE_SCLR_REG              = 0x05 # Status Clear Register
GE_AVE_REG               = 0x07 # Setting moving average Output Mode
GE_INTHL_REG             = 0x08 # Interrupt upper value - Upper level
GE_INTHH_REG             = 0x09 # Interrupt upper value - Upper level
GE_INTLL_REG             = 0x0A # Interrupt lower value - Lower level
GE_INTLH_REG             = 0x0B # Interrupt lower value - upper level
GE_IHYSL_REG             = 0x0C # Interrupt hysteresis value - Lower level
GE_IHYSH_REG             = 0x0D # Interrupt hysteresis value - Lower level
GE_TTHL_REG              = 0x0E # Thermistor Output Value - Lower level
GE_TTHH_REG              = 0x0F # Thermistor Output Value - Upper level
GE_INT0_REG              = 0x10 # Pixel 1->8    Interrupt Result 
GE_INT1_REG              = 0x11 # Pixel 9->16   Interrupt Result  
GE_INT2_REG              = 0x12 # Pixel 17->24  Interrupt Result 
GE_INT3_REG              = 0x13 # Pixel 25->32  Interrupt Result 
GE_INT4_REG              = 0x14 # Pixel 33->40  Interrupt Result
GE_INT5_REG              = 0x15 # Pixel 41->48  Interrupt Result  
GE_INT6_REG              = 0x16 # Pixel 49->56  Interrupt Result
GE_INT7_REG              = 0x17 # Pixel 57->64  Interrupt Result
GE_AVE_REG2              = 0x1F # Setting moving average Output Mode

GE_PIXEL_BASE            = 0x80 # 温度レジスタ 基底値 Pixel 1 Output Value (Lower Level)

#-------------------------------------------------------------------------------
# レジスタ 設定値
#-------------------------------------------------------------------------------
# パワーコントロール レジスタ(動作モード設定)
# GE_POWER_CTL_REG
GE_PCTL_NORMAL_MODE         = 0x00 
GE_PCTL_SLEEEP_MODE         = 0x10 
GE_PCTL_STAND_BY_60S_MODE   = 0x20
GE_PCTL_STAND_BY_10S_MODE   = 0x21

# GE_RESET_REG - Software Resets
GE_RST_FLAG_RST         = 0x30
GE_RST_INITIAL_RST      = 0x3F

# GE_FPSC_REG - Sample Rate Settings
GE_FPSC_1FPS            = 0x01
GE_FPSC_10FPS           = 0x00

# GE_INT_CTL_REG - Interrupt Settings
GE_INTC_ABS             = 0b00000001
GE_INTC_DIF             = 0b00000011
GE_INTC_OFF             = 0b00000000

# GE_AVE_REG - average Settings
GE_ABE_NOMAL            = 0x00
GE_ABE_MAMOD            = 0x20

# GE_STAT_REG - Overflow/Interrupt Settings
GE_SCLR_CLR             = 0b00000110

# GE_PIXEL_BASE - 温度レジスタ Settings
_PIXEL_ARRAY_WIDTH      = 8
_PIXEL_ARRAY_HEIGHT     = 8
_PIXEL_TEMP_CONVERSION  = 0.25
_THERMISTOR_CONVERSION  = 0.0625

class AMG8833(object):
#*******************************************************************************
# コンストラクタ
#*******************************************************************************
    def __init__(self,
                 ft232h, num, address, clock_hz,
                 rvs_pxl    = False,
                 use_hi_pxl = False,
                 use_hi_thm = False):

        # I2C オブジェクト生成
        self.i_iNum       = num
        self.i_iAddr      = address
        self.i_iClockHz   = clock_hz
        self.i_cI2C = FT232H.I2CDevice( ft232h   = ft232h,          \
                                        address  = self.i_iAddr,    \
                                        clock_hz = self.i_iClockHz)

        self.i_bReversePixel        = rvs_pxl
        self.i_bUsePixelTempHigh    = use_hi_pxl
        self.i_bUseThermTempHigh    = use_hi_thm

        # イニシャライズ
        self.__Init()
        return

#*******************************************************************************
# デストラクタ
#*******************************************************************************
    def __del__(self):
        return

#*******************************************************************************
# イニシャライズ
#*******************************************************************************
    def __Init(self):

        # config設定
        self.__SetSensorMode(GE_PCTL_NORMAL_MODE)   # パワーコントロール モード設定
        time.sleep(50/1000)     # 50ミリ秒待機

        self.__ResetFlags(GE_RST_INITIAL_RST)       # ソフトウェアリセット情報 設定
        time.sleep(2/1000)      # 2ミリ秒待機

        self.__SetInterruptMode(GE_INTC_OFF)        # 割込み モード設定
        self.__SetFrameRate(GE_FPSC_10FPS)          # フレームレート 設定
        self.__SetAverageMode(GE_ABE_MAMOD)         # 移動平均出力モード設定

        time.sleep(100/1000)    # 100ミリ秒待機(ガード)

        return

#*******************************************************************************
# パワーコントロール モード設定
#*******************************************************************************
    def __SetSensorMode(self, mode):
        self.i_cI2C.write8(GE_POWER_CTL_REG, mode)  # mode
        return

#*******************************************************************************
#  ソフトウェアリセット情報 設定
#*******************************************************************************
    def __ResetFlags(self, value):
        self.i_cI2C.write8(GE_RESET_REG, value)     # reset
        return

#*******************************************************************************
#  フレームレート 設定
#*******************************************************************************
    def __SetFrameRate(self, value):
        self.i_cI2C.write8(GE_FPSC_REG, value)      # sample rate
        return

#*******************************************************************************
# 割込み モード設定
#*******************************************************************************
    def __SetInterruptMode(self, mode):
        self.i_cI2C.write8(GE_INT_CTL_REG, mode)    # interrupts
        return

#*******************************************************************************
#  ステータス クリア
#*******************************************************************************
    def __ClearStatus(self, value):
        self.i_cI2C.write8(GE_SCLR_REG, value)      # overflows
        return

#*******************************************************************************
#  移動平均出力モード設定
#*******************************************************************************
    def __SetAverageMode(self, mode):
        self.i_cI2C.write8(GE_AVE_REG2,   0x50)
        self.i_cI2C.write8(GE_AVE_REG2,   0x45)
        self.i_cI2C.write8(GE_AVE_REG2,   0x57)
        self.i_cI2C.write8(GE_AVE_REG,    mode)
        self.i_cI2C.write8(GE_AVE_REG2,   0x00)
        return

#*******************************************************************************
# conversion for pixels
#*******************************************************************************
    def Detail(self):

        str = "{ GE=%d, SlaveAddr: 0x%02X, Config:{ ReversePixel: %r, UsePixelTempHigh: %r, UseThermTempHigh: %r } }" %(       \
                            self.i_iNum,                \
                            self.i_iAddr,               \
                            self.i_bReversePixel,       \
                            self.i_bUsePixelTempHigh,   \
                            self.i_bUseThermTempHigh)
        return str

#*******************************************************************************
# conversion for pixels
#*******************************************************************************
    def twos_compl(self, val):

        # 氷点下か?
        if  ((0x7FF & val) == val):
            return float(val)
        else:
            return float(val - 4096)

#*******************************************************************************
# conversion for thermistor
#*******************************************************************************
    def signed_conv(self, val):

        # 氷点下か?
        if ((0x7FF & val) == val):
            return float(val)
        else:
            return -float(0x7FF & val)

#*******************************************************************************
# 温度レジスタ 情報取得
#*******************************************************************************
    def ReadPixels(self):

        #arTemp = [[0] * _PIXEL_ARRAY_WIDTH for _ in range(_PIXEL_ARRAY_HEIGHT)]
        arTemp = [0] * (_PIXEL_ARRAY_HEIGHT * _PIXEL_ARRAY_WIDTH)

        for iRow in range(0, _PIXEL_ARRAY_HEIGHT):      # 行ループ
            for iCol in range(0, _PIXEL_ARRAY_WIDTH):   # 列ループ
                # 温度レジスタ アドレス 生成
                iRegOffSet = iRow * _PIXEL_ARRAY_HEIGHT + iCol
                iReg = GE_PIXEL_BASE + (iRegOffSet << 1)    # 0x80(T01L), 0x82(T02L), 0x83(T03L) ～ 0xFE(T64L) 一つ跳ばし(下位情報のみ)  

                # 温度レジスタ情報 取得(下位)
                iTempRawL  = self.i_cI2C.readU8(iReg)

                # 温度レジスタ情報 取得(上位)
                if (self.i_bUsePixelTempHigh == True):
                    iTempRawH = self.i_cI2C.readU8(iReg + 1)
                else:
                    iTempRawH = 0

                iTempRaw = ((iTempRawH & 0xFF) << 8) | iTempRawL

                # 温度レジスタ情報 補正
                fTempConv = self.twos_compl(iTempRaw) * _PIXEL_TEMP_CONVERSION  # scaling values 0.25

                # 温度情報 記憶
                #arTemp[iRow][iCol] = fTempConv
                arTemp[iRegOffSet] = fTempConv

        return True, arTemp

#*******************************************************************************
# サーミスタ 情報取得
#*******************************************************************************
    def ReadThermistor(self):
        iThmRawH = self.i_cI2C.readU16(GE_TTHH_REG) # read thermistor (background temp)

        if (self.i_bUseThermTempHigh == True):
            iThmRawL = self.i_cI2C.readU16(GE_TTHL_REG)  # read thermistor (background temp)
        else:
            iThmRawL = 0

        iThmRawHL = (iThmRawH << 8) | iThmRawL
        fThmConv = self.signed_conv(iThmRawHL) * _THERMISTOR_CONVERSION # scaling values 0.0625

        return fThmConv

#*******************************************************************************
# End Of File
#*******************************************************************************
