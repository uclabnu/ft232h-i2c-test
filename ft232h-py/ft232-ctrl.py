# coding: utf-8

# Import standard Python time library.
import time

# Import GPIO and FT232H modules.
import UsbI2cBrd as UsbI2C

#*******************************************************************************
# 接続デバイス 検索
#*******************************************************************************
def SearchI2CDevice():

    # 接続デバイス 検索
    bRet, lsDeviceList = g_cUsbI2C.GetI2CSlaveDeviceList()
    if (bRet != True):
        return False

    # 接続デバイス数 チェック
    iRet = len(lsDeviceList)
    if (iRet <= 0):
        return False

    return True

#*******************************************************************************
# main
#*******************************************************************************
if __name__ == '__main__':
    global g_cUsbI2C
    g_cUsbI2C = UsbI2C.USB_I2CBoard()

    while True: # 無限ループ

        # 接続I2Cデバイス 検索
        bRet = SearchI2CDevice()
        if (bRet != True):
            print ('Sensor not found')
            time.sleep(2)   # 2秒待機
            continue        # whileループ先頭へ

        # GridEye起動 (処理無限ループ)
        bRet, iErr = g_cUsbI2C.StartupGridEye()
        if (bRet != True):
            if (iErr == 99):
                break

    exit(0)

#*******************************************************************************
# End Of File
#*******************************************************************************
