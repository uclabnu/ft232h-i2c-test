# coding: utf-8
# Import standard Python time library.
import time

# Import GPIO and FT232H modules.
import Adafruit_GPIO.FT232H as FT232H
import AMG8833 as GridEye


class USB_I2CBoard (object):
#*******************************************************************************
# クラス定数 定義
#*******************************************************************************
    # I2C アドレス限界
    I2C_ADDR_MIN = 0x08     # 0x08(8)    I2Cスレーブアドレス 下限
    I2C_ADDR_MAX = 0x77     # 0x77(119)  I2Cスレーブアドレス 上限

    # I2C クロック周波数
    I2C_CLK_HZ_100K = 100000    # standard mode
    I2C_CLK_HZ_400K = 400000    # fast mode
    I2C_CLK_HZ_1M   = 1000000   # fast mode plus
    I2C_CLK_HZ      = I2C_CLK_HZ_400K   # Grid-EYEは、fast mode 対応

#*******************************************************************************
# コンストラクタ
#*******************************************************************************
    def __init__(self):

        #rospy.loginfo('I2CDevice start')

        #-----------------------------------------------------------------------
        # ｲﾝｽﾀﾝｽ変数定義
        #-----------------------------------------------------------------------
        self.i_cFTDI_DEV        = None

        # Temporarily disable the built-in FTDI serial driver on Mac & Linux platforms.
        FT232H.use_FT232H()

        bRet = self.__DeviceInit()
        return

#*******************************************************************************
# デストラクタ
#*******************************************************************************
    def __del__(self):
        # 動作中に Ctrl+C などで強制終了すると、ボード動作が不安定になるので
        # 初期化のため再接続する
        #（i_cFTDI_DEVが使えなくなっているので __GpioPinInit(）直接コールできない）
        bRet = self.__DeviceReConnect()

        bRet = self.__DeviceClose()

        #rospy.loginfo('I2CDevice stop')
        return

#*******************************************************************************
# イニシャライズ
#*******************************************************************************
    def __DeviceInit(self):

        self.i_cFTDI_DEV = None
        bRet = self.__DeviceFound()
        if (bRet != True):
            return False

        return True

#*******************************************************************************
# デバイス 接続チェック
#*******************************************************************************
    def __DeviceFound(self):

        bTryErr = False

        if (self.i_cFTDI_DEV != None):  # IsDeviceUse() は使用しない（再帰呼出し対策）
            return True

        try:
            # Create an FT232H object that grabs the first available FT232H device found.
            self.i_cFTDI_DEV = FT232H.FT232H()

        except RuntimeError as err:
            #rospy.loginfo('Device Found Error(RuntimeError)')
            bTryErr = True

        except:
            #rospy.loginfo('Device Found Error(misc)')
            bTryErr = True

        if (bTryErr == True):
#            time.sleep(1.0)    # タイマ（仮 パイプバッファ オーバフロー対策）
            return False        # all False

        if (self.i_cFTDI_DEV == None):  # IsDeviceUse() は使用しない（再帰呼出し対策）
            return False

        return True

#*******************************************************************************
# デバイス クローズ
#*******************************************************************************
    def __DeviceClose(self):

        bTryErr = False

        if (self.i_cFTDI_DEV == None):  # IsDeviceUse() は使用しない（再帰呼出し対策）
            # クローズできないので、正常終了扱いとする
            return True

        try:
            self.i_cFTDI_DEV.close()
            del self.i_cFTDI_DEV

        except RuntimeError as err:
            #rospy.loginfo('Device Close Error(RuntimeError)')
            bTryErr = True

        except :
            #rospy.loginfo('Device Close Error(misc)')
            bTryErr = True

        self.i_cFTDI_DEV = None

        if (bTryErr == True):
            return False

        return True


#*******************************************************************************
# デバイス 再接続
#*******************************************************************************
    def __DeviceReConnect(self):

        bRet = self.__DeviceClose()
        if (bRet != True):
            return False

        bRet = self.__DeviceInit()

        return bRet

#*******************************************************************************
# デバイス 利用可否チェック
#       dev_found | True    :   利用可否をチェックし、利用不可の場合、デバイス初期化を行う
#                 | False   :   利用可否のみをチェックする
#*******************************************************************************
    def __IsDeviceUse(self, dev_found = True):


        if (self.i_cFTDI_DEV != None):
            return True

        # 接続不要か?
        if (dev_found != True):
            return False

        bRet = self.__DeviceInit()
        return bRet

#*******************************************************************************
# I2C スレーブデバイス アドレス 一覧取得
#*******************************************************************************
    def GetI2CSlaveDeviceList(self):

        lsAddrList = list()  # empty配列

        # デバイス 利用可否チェック
        bRet = self.__IsDeviceUse(dev_found = True)
        if (bRet != True):
            return False, lsAddrList

        bTryErr = False

        try:
            for iAddr in range(127):
                # Skip I2C addresses which are reserved.
                if (iAddr < self.I2C_ADDR_MIN):
                    continue        # forループ先頭へ

                if (iAddr > self.I2C_ADDR_MAX):
                    continue        # forループ先頭へ

                # Create I2C object.
                cI2C = FT232H.I2CDevice( ft232h   = self.i_cFTDI_DEV,       \
                                         address  = iAddr,                  \
                                         clock_hz = self.I2C_CLK_HZ)

                # Check if a device responds to this address.
                bRet = cI2C.ping()
                if (bRet != False):
                    lsAddrList.append(iAddr) # アドレスリストに追加
                    #print 'Found I2C device at address 0x{0:02X}'.format(iAddr)

        except RuntimeError as err:
            #print('GetSlaveDeviceList Error(RuntimeError)')
            bTryErr = True

        except :
            #print('GetSlaveDeviceList Error(misc)')
            bTryErr = True

        if (bTryErr == True):
            bRet = self.__DeviceReConnect()
            return False, lsAddrList    # all False

        return True, lsAddrList

#*******************************************************************************
# Grid-EYE 動作開始
#*******************************************************************************
    def StartupGridEye(self):

        # デバイス 利用可否チェック
        bRet = self.__IsDeviceUse(dev_found = True)
        if (bRet != True):
            return False, 0

        # I2C スレーブデバイス アドレス一覧取得
        bRet, lsAddrList = self.GetI2CSlaveDeviceList()
        if (bRet != True):
            return False, 0

        # I2C スレーブデバイス アドレス数チェック
        iAddrListCnt = len(lsAddrList)
        if (iAddrListCnt <= 0):
            return True

        iTryErr = 0
        iStep = 0

        try:
            cGE_List = list()  # empty配列

            # GEクラスリスト 生成
            iNum = 0
            for iAddr in lsAddrList:
                # AMG8833(GridEye) クラス生成
                cGE = GridEye.AMG8833( ft232h   = self.i_cFTDI_DEV,         \
                                       num      = iNum,                     \
                                       address  = iAddr,                    \
                                       clock_hz = self.I2C_CLK_HZ)
                det = cGE.Detail()
                print ('using: {0:s}'.format(det))
                cGE_List.append(cGE)    # GEクラスリストに追加
                iNum += 1

            iStep += 1

            while True: # 無限ループ
                for iNum in range(iAddrListCnt):
                    t1 = time.time()
                    # 温度情報取得 (8×8 ピクセル)
                    bRet, arTemp = cGE_List[iNum].ReadPixels()
                    if (bRet != False):
                        t2 = round((time.time() - t1) * 1000)
                        # print cGE_List[iNum].iarTemp
                        print ('GE={0:d}(0x{1:02X}) time={2:f} ms {3:s}'.format(iNum, cGE_List[iNum].i_iAddr, t2, arTemp))
        except RuntimeError as err:
            #print('StartupGridEye Error(RuntimeError)')
            if (iStep > 0):
                print (err)
                print ('Not found I2C device at address 0x{0:02X}'.format(lsAddrList[iNum]))
                iTryErr = 99    # 既存lsAddrListとの整合性を考慮し、処理中断させる
            else:
                iTryErr = 1

        except :
            # 無限ループで Ctrl+C するとここに来る
            #print('StartupGridEye Error(misc)')
            iTryErr = 99

        if (iTryErr != 0):
            bRet = self.__DeviceReConnect()
            return False, iTryErr

        return True, 0

#*******************************************************************************
# End Of File
#*******************************************************************************
