module ft232h-go-ardnew

go 1.17

require (
	github.com/ardnew/ft232h v0.0.0-20201107182047-b95250269fed
	local.packages/grideye v0.0.0-00010101000000-000000000000
)

replace local.packages/grideye => ./grideye
