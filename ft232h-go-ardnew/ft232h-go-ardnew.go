package main

//******************************************************************************
// import
//******************************************************************************
import (
	"C"
	"flag"

	//"fmt"
	"log"
	"os/exec"
	"runtime"
	"sync"
	"time"

	ft232h "github.com/ardnew/ft232h"
	grideye "local.packages/grideye"
)

//******************************************************************************
// 起動パラメタ他 定義
//******************************************************************************
var (
	paraRvsPixel     = flag.Bool("rvs_pxl", false, "Reverse setting of Pixels data")
	paraUseHighPixel = flag.Bool("use_hi_pxl", false, "Use of high digits in Int Pixels data")
	paraUseHighTherm = flag.Bool("use_hi_thm", false, "Use of high digits in Int Thermistor data")
	mu               sync.Mutex
	version          = "0.01"
)

//******************************************************************************
// I2C設定情報作成
//******************************************************************************
func CreateI2CConfig() (*ft232h.I2CConfig, error) {
	//------------------------------------------------------
	// Config初期値取得
	//------------------------------------------------------
	i2ccfg := ft232h.I2CConfigDefault()

	//------------------------------------------------------
	// Option
	//------------------------------------------------------
	// BreakOnNACK（default: false）
	//	false:	デバイスからのACKまたはNACKを気にせずに、データをまとめて送信し続ける
	//	true:	デバイスがNACKしたときに、バッファ内のデータの送信を停止しする
	i2ccfg.I2COption.BreakOnNACK = true

	// LastReadNACK（default: false）
	//	libMPSSEは、読み取られたバイトごとにACKを生成します。
	//	一部のI2Cスレーブ機器では、最後に読み取られたデータバイトのNACKを生成するためにI2Cマスターが必要。
	//	このビットを設定すると、そのようなI2Cスレーブでの作業が可能になる
	i2ccfg.I2COption.LastReadNACK = true // GRID-Eyeはtrue設定

	// NoUSBDelay（default: true）
	//	すべてのI2Cデータを最小数のUSBパケットにパックする／しない
	//	※ trueにすると不正スレーブアドレスへのコマンドでも正常扱いになる
	i2ccfg.I2COption.NoUSBDelay = false // GRID-Eyeはfalse設定

	//------------------------------------------------------
	// Clock（default: I2CClockFastMode(400 kb/sec)）
	// 100000 (100 kb/s) - 3400000 (3.4 Mb/s)
	//	 I2CClockStandardMode  100 kb/sec
	//	 I2CClockFastMode	   400 kb/sec
	//	 I2CClockFastModePlus  1000 kb/sec
	//	 I2CClockHighSpeedMode 3.4 Mb/sec
	//------------------------------------------------------
	i2ccfg.Clock = ft232h.I2CClockFastMode

	//------------------------------------------------------
	// Latency（default: 2）
	// レイテンシータイマーに必要な値（ミリ秒単位）。
	// 有効な範囲は0〜255です。（0はdefaultに書替えられる）
	// ただし、FTDIでは、レイテンシータイマーに次の範囲の値を推奨しています。
	//	Full speed(480Mbps) devices (FT2232D)					Range 2 – 255
	//	Hi speed(12Mbps) devices	(FT232H, FT2232H, FT4232H)	Range 1 - 255
	//------------------------------------------------------
	i2ccfg.Latency = 1

	//------------------------------------------------------
	// Clock3Phase（default: true）
	// I2C 3-phase clocking enabled=true/disabled=false
	//------------------------------------------------------
	i2ccfg.Clock3Phase = true

	//------------------------------------------------------
	// LowDriveOnly（default: true）
	// float HIGH (pullup) if true, drive HIGH if false
	//------------------------------------------------------
	i2ccfg.LowDriveOnly = true

	//------------------------------------------------------
	// 上記以外はdefault使用
	//------------------------------------------------------

	return i2ccfg, nil
}

//******************************************************************************
// 温度レジスタ 情報取得（仮対策：goルーチンテストのため関数化）
//******************************************************************************
func readPixels(ge *grideye.GRIDEYE) {

	start := time.Now()
	Pixels, err := ge.ReadPixels()
	if nil != err {
		log.Fatalf("grideye.GetPixels(): %v", err)
	}
	end := time.Now()
	log.Printf("Pixels GE=%d(0x%02X) time=%s : %g", ge.GENum, ge.SlaveAddr, end.Sub(start), Pixels)
}

//******************************************************************************
// サブプロセス呼出し
//******************************************************************************
func SubProcessCall(cmd string) ([]byte, error) {

	sCmdResult, err := exec.Command("sh", "-c", cmd).Output()
	if err != nil {
		return nil, err
	}

	return sCmdResult, nil
}

//******************************************************************************
// FTDI ドライバ無効
//******************************************************************************
func disable_FTDI_driver() error {

	os := runtime.GOOS
	switch os {
	case "darwin":
		// エラーチェックしない
		SubProcessCall("kextunload -b com.apple.driver.AppleUSBFTDI")
		SubProcessCall("kextunload /System/Library/Extensions/FTDIUSBSerialDriver.kext")

	case "linux":
		sPass := "uclab"
		sSudo := "echo " + sPass + " | sudo -S "

		// エラーチェックしない(root権限がないとエラーになる（後で検討）)
		SubProcessCall(sSudo + "modprobe -r -q ftdi_sio")
		SubProcessCall(sSudo + "modprobe -r -q usbserial")

	case "windows":
	case "freebsd":
	default: // ガード

	}

	return nil
}

//******************************************************************************
// FTDI ドライバ有効
//******************************************************************************
func enable_FTDI_driver() error {

	os := runtime.GOOS
	switch os {
	case "darwin":
		// エラーチェックしない
		SubProcessCall("kextload -b com.apple.driver.AppleUSBFTDI")
		SubProcessCall("kextload /System/Library/Extensions/FTDIUSBSerialDriver.kext")

	case "linux":
		sPass := "uclab"
		sSudo := "echo " + sPass + " | sudo -S "

		// エラーチェックしない
		SubProcessCall(sSudo + "modprobe -q ftdi_sio")
		SubProcessCall(sSudo + "modprobe -q usbserial")

	case "windows":
	case "freebsd":
	default: // ガード

	}

	return nil
}

//******************************************************************************
// メインルーチン
//******************************************************************************
func main() {
	var err error

	//------------------------------------------------------
	// 起動パラメタ処理
	//------------------------------------------------------
	flag.Parse()
	//fmt.Println(*paraRvsPixel, *paraUseHighPixel, *paraUseHighTherm)

	//------------------------------------------------------
	// FTDIドライバアンロード
	//------------------------------------------------------
	err = disable_FTDI_driver()
	if nil != err {
		// sudo rmmod ftdi_sio usbserial
		log.Fatalf("disable_FTDI_driver(): %s", err)
	}
	defer enable_FTDI_driver()

	//------------------------------------------------------
	// FT232H オブジェクト生成
	//------------------------------------------------------
	var ft *ft232h.FT232H
	ft, err = ft232h.New()
	if nil != err {
		// VCPドライバがロードされているとエラーになるので注意
		// sudo rmmod ftdi_sio usbserial
		log.Fatalf("ft232h.New(): %s", err)
	}

	defer ft.Close()
	//log.Printf("%s\n", ft)

	//------------------------------------------------------
	// FT232H（I2C通信） オブジェクト生成
	//------------------------------------------------------
	// I2C設定情報作成
	var i2ccfg *ft232h.I2CConfig
	i2ccfg, err = CreateI2CConfig()
	if nil != err {
		log.Fatalf("CreateI2CConfig(): %s", err)
	}

	// initialize FT232H MPSSE engine in I2C mode
	// （Config設定とInit()を同時に行う）
	err = ft.I2C.Config(i2ccfg)
	//err = ft.I2C.Init();
	if nil != err {
		log.Fatalf("I2C.Config(): %v", err)
	}

	//defer ft.I2C.Close()		// ←たぶん不要

	log.Printf("FTDI info: %s", ft) // FT232H implements String()

	//------------------------------------------------------
	// I2C デバイス検索
	//------------------------------------------------------
	var iSlaveAddr int
	var iSlaveAddrList []int
	//var uiWrtSize		uint
	uiDummy := []uint8{0x00}
	//uiDummy := []uint8{0x80}

	for iSlaveAddr = ft232h.I2CSlaveAddressMin; iSlaveAddr <= ft232h.I2CSlaveAddressMax; iSlaveAddr++ {
		//// レジスタアドレス書込み
		//uiWrtSize, err = ft.I2C.Write(uint(iSlaveAddr), uiDummy, true, true)	// stopビットはfalseでも下位で書込サイズで自動判定する!?
		//if (nil != err) {
		//	print (err)
		//	continue
		//}
		//log.Printf("ft.I2C.Write (addr:0x%02x, size:%d)\n", iSlaveAddr, uiWrtSize)
		//
		////レジスタアドレス情報 読込み（8ビット）
		//ReadDt, err := ft.I2C.Read(uint(iSlaveAddr), uint(ft232h.Addr8Bit), true, true)	// stopビットはfalseでも下位で読込サイズで自動判定する!?
		//if (nil != err) {
		//	print (err)
		//	continue
		//}
		//log.Printf ("ReadDt size:%d\n",len(ReadDt))

		_, err = ft.I2C.Write(uint(iSlaveAddr), uiDummy, true, true)
		//log.Printf("ft.I2C.Write (addr:0x%02x, size:%d)\n", iSlaveAddr, uiWrtSize)
		if nil != err {
			// 存在しないI2Cアドレスへのコマンドはここに来る
			// ※ I2COption.NoUSBDelay = false に設定すること
			continue // for文 先頭へ
		}

		iSlaveAddrList = append(iSlaveAddrList, iSlaveAddr) // アドレスリストに追加
	}
	log.Printf("Find I2C Device Count = %d\n", len(iSlaveAddrList))

	//------------------------------------------------------
	// grideyeオブジェクト生成
	//------------------------------------------------------
	var iCnt int
	var geCfg *grideye.GEConfig
	var pGEList []*grideye.GRIDEYE

	// config 設定
	geCfg = grideye.GetConfigDefault()
	geCfg.ReversePixel = *paraRvsPixel
	geCfg.UsePixelTempHigh = *paraUseHighPixel
	geCfg.UseThermTempHigh = *paraUseHighTherm

	// grideyeオブジェクト生成
	for iCnt, iSlaveAddr = range iSlaveAddrList {
		//log.Printf(" addr %02d: 0x%02x\n", iCnt, iSlaveAddr)
		ge, err := grideye.New(uint(iCnt), uint(iSlaveAddr), ft.I2C, geCfg)
		if nil != err {
			log.Fatalf("grideye.New(): %v, GE=%d(0x%02X)", err, iCnt, iSlaveAddr)
		}

		log.Printf("using: %s", ge)   // GRIDEYE implements String()
		pGEList = append(pGEList, ge) // GRIDEYEリストに追加
	}

	//------------------------------------------------------
	// レジスタ 情報取得
	//------------------------------------------------------
	for { // 無限ループ
		for iCnt = 0; iCnt < len(pGEList); iCnt++ {
			//--------------------------------------------------
			// 温度レジスタ 情報取得
			//--------------------------------------------------
			readPixels(pGEList[iCnt])
			//Pixels, err := pGEList[iCnt].ReadPixels()
			//if (nil != err) {
			//	log.Fatalf("grideye.GetPixels(): %v", err)
			//}
			//log.Printf("Pixels (GE=%d): %g", pGEList[iCnt].GENum, Pixels)

			//--------------------------------------------------
			// サーミスタ 情報取得（未使用データ）
			//--------------------------------------------------
			//Therm, err := pGEList[iCnt].ReadThermistor()
			//if (nil != err) {
			//	log.Fatalf("grideye.ReadThermistor(): %v", err)
			//}
			//log.Printf("Therm (GE=%d): %f", pGEList[iCnt].GENum, Therm)
		}
	}

	return
}

//******************************************************************************
// End Of File
//******************************************************************************
